% Binomial Max Heap

% a binomial heap is a list of binomial trees
% the list elements increase in tree order

% a binomial tree is a tuple (order, value, list)
% this list is a list of sub trees in increasing order too

% for the operations see e.g.
%   Atul S. Khot and Raju Kumar Mishra: 
%   Learning Functional Data Structures and Algorithms
%   Packt Publishing, 2017

-module(binomial_max_heap).
-export([
	 heap_new/0, 
	 heap_merge/2,
	 heap_insert/2,
	 heap_max/1,
	 heap_extract_max/1
	]).


% heap_new()
heap_new() ->
  [].


% heap_merge(Heap1, Heap2)
heap_merge(Heap1, []) ->
  Heap1;
heap_merge([], Heap2) ->
  Heap2;
heap_merge([{K1, _X1, _L1}=Tree1|T1], [{K2, _X2, _L2}|_T2]=Heap2) when K1 < K2 ->
  [Tree1|heap_merge(T1, Heap2)];
heap_merge([{K1, _X1, _L1}|_T1]=Heap1, [{K2, _X2, _L2}=Tree2|T2]) when K2 < K1 ->
  [Tree2|heap_merge(Heap1, T2)];
heap_merge([Tree1|T1], [Tree2|T2]) ->
  Tree = tree_merge(Tree1, Tree2),
  Heap = heap_merge(T1, T2),
  heap_insert_tree(Tree, Heap).


% heap_insert_tree(Tree, Heap)
heap_insert_tree(Tree, []) ->
  [Tree];
heap_insert_tree({K1, _X1, _L1}=Tree, [{K2, _X2, _L2}|_T2]=Heap) when K1 < K2 ->
  [Tree|Heap];
heap_insert_tree(Tree1, [Tree2|T2]) ->
  Tree = tree_merge(Tree1, Tree2),
  heap_insert_tree(Tree, T2).
 
 
% tree_merge(Tree1, Tree2)
tree_merge({K, X1, L1}, {K, X2, _L2}=Tree2) when X1 >= X2 ->
  {K+1, X1, L1 ++ [Tree2]};
tree_merge({K, _X1, _L1}=Tree1, {K, X2, L2}) ->
  {K+1, X2, L2 ++ [Tree1]}.


% heap_insert(X, Heap)
heap_insert(X, Heap) ->
  Tree = {0, X, []},
  heap_insert_tree(Tree, Heap).


% heap_max(Heap)
heap_max(Heap) ->
  [{_K0, X0, _L0}|_T]=Heap,
  lists:foldl(fun({_K, X, _L}, Max) ->
    max(Max, X)
  end, X0, Heap).


% heap_extract_max(Heap)
heap_extract_max(Heap) ->
  {Max, TreeMax} = heap_max_tree(Heap),
  {KMax, Max, LMax} = TreeMax,
  Heap2 = lists:filter(fun({K, _X, _L}) ->
    K =/= KMax
  end, Heap),
  Heap3 = heap_merge(Heap2, LMax),
  {Max, Heap3}.


heap_max_tree(Heap) ->
  [{_K0, X0, _L0}=Tree0|_T]=Heap,
  lists:foldl(fun({_K, X, _L}=Tree, {Max, TreeMax}) ->
    case X > Max of
      true ->
	{X, Tree};
      false ->
	{Max, TreeMax}
    end
  end, {X0, Tree0}, Heap).
